import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import VideoPlayer from '../VideoPlayer';
import dataMock from './videoDataMock.json';

const styles = {
  videoComponent: {
    display: "flex",
    flexWrap: "wrap",
    margin: "0 auto",
  },
  player:{
    margin: 10,
    minWidth: 350,
    flex: "1 0 32%"
  },
  headerTitle: {
    padding: 20
  },
  widgetTitle:{
    textTransform: "uppercase",
    margin: "10px 0",
    borderLeft: "4px solid #f39c12",
    paddingLeft: 10,
    fontVariant: "small-caps",
    fontWeight: "bold",
    color: "#2196F3",
    borderBottom: "1px solid #ccc",
    fontFamily: "Roboto",
    fontSize: 18
  }
};

function MediaCard(props) {
  const { classes } = props;
  return (
    <React.Fragment>
      <div className = {classes.headerTitle}>
        <h3 className={classes.widgetTitle}>Học tiếng nhật cùng Jellyfish</h3>
      </div>
      <div className = {classes.videoComponent}>
        {dataMock.map((data, index) => (
          <div key = {index} className = {classes.player}>
            <VideoPlayer
              url = {data.url}
              name = {data.name}
              content = {data.content}
            />
          </div>
          )
        )}
        </div>
    </React.Fragment>
    
  );
}

MediaCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MediaCard);
