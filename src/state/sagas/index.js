
import { fork, all } from 'redux-saga/effects'

import { routes } from './routerSagas'
import auth from '../modules/auth/sagas'

export function * sagas () {
  yield all([
    yield fork(routes),
    yield fork(auth)
  ])
}
